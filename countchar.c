#include <stdio.h>

int compte_char(char *chaine, char cherche) {
 int nombre = 0;

 for (int i = 0; chaine[i] != '\0'; i++)
  if (chaine[i] == cherche) {
    nombre++;
         }

 return nombre;
}

int main {
 char chaine[50];
 char cherche;

 printf("Entrez un mot : ");
 scanf("%s", chaine);

 printf("Entrez un caractère à chercher dedans : ");
 scanf(" %c", &cherche);

 printf("Il y a %d fois le caractère dedans \n", compte_char(chaine, cherche));

 return 0;
}