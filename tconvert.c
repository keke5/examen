#include <stdio.h>

float fahrenheit2celsius (float fahrenheit) {
 float celsius;

 celsius = ((fahrenheit - 32) * 5/9);

 return celsius;
}

float celsius2fahrenheit (float celsius) {
 float fahrenheit;

 fahrenheit = (1.8 * celsius) + 32;
 
 return fahrenheit;
}

int main {
 float degre;
 int choix;

 printf("\nChoisissez une conversion (1 ou 2) : ");
 scanf("%d", &choix);
 if (choix == 1) {
    printf("Quel degré celsius: ");
    scanf("%f", &degre);
    degre = celsius2fahrenheit(degre);
    printf("En fahrenheit c’est égal à : %f \n", degre);
   }
   if (choix == 2) {
     printf("Quel degré fahrenheit: ");
     scanf("%f", &degre);
     degre = fahrenheit2celsius(degre);
     printf("En Celsius c’est égal à : %f °C\n", degre);
    }
    return 0;
}
